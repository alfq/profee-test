import { Injectable } from "@angular/core";
import { InMemoryDbService } from "angular-in-memory-web-api";
import { User } from "./model/user";

@Injectable({
  providedIn: "root"
})
export class DataService implements InMemoryDbService {
  constructor() {}

  createDb() {
    const users: User[] = [
      { id: 1, name: "John Doe", phone: 123, description: "Manager" },
      { id: 2, name: "Jemima Mor", phone: 345, description: "CEO" },
      { id: 3, name: "Andrey Drey", phone: 678, description: "Employee" }
    ];

    return { users };
  }
}
