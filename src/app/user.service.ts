import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "./model/user";

@Injectable({
  providedIn: "root"
})
export class UserService {
  users_url = "api/users";

  constructor(private http: HttpClient) {}

  public getUsers() {
    return this.http.get<User[]>(this.users_url);
  }

  public getUser(userId: number) {
    return this.http.get<User>(`${this.users_url}/${userId}`);
  }

  public createUser(user: User) {
    return this.http.post<User>(this.users_url, user);
  }

  public updateUser(user: User) {
    return this.http.put<User>(`${this.users_url}/${user.id}`, user);
  }

  public deleteUser(userId: number) {
    return this.http.delete<User>(`${this.users_url}/${userId}`);
  }
}
