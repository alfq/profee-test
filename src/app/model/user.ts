export interface User {
  id: number;
  name: string;
  phone: number;
  description: string;
}
