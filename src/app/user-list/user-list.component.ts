import { UserService } from "./../user.service";
import { Component, OnInit } from "@angular/core";
import { User } from "../model/user";
import { Observable, of } from "rxjs";
import { Router } from "@angular/router";
import { mergeMap } from "rxjs/operators";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"]
})
export class UserListComponent implements OnInit {
  users$: Observable<User[]>;

  constructor(private userService: UserService, private router: Router) {
    this.users$ = userService.getUsers();
  }

  ngOnInit() {}

  editUser(user: User) {
    this.router.navigateByUrl("/add-user", { state: user });
  }

  deleteUser(userId: number) {
    this.userService
      .deleteUser(userId)
      .pipe(mergeMap(() => this.users$))
      .subscribe(users => (this.users$ = of(users)));
  }
}
