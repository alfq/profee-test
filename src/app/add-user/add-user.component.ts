import { UserService } from "./../user.service";
import { Component, OnInit } from "@angular/core";
import { User } from "../model/user";
import { Router, ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"]
})
export class AddUserComponent implements OnInit {
  userState$: Observable<User>;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.userState$ = this.route.paramMap.pipe(map(() => window.history.state));
  }

  onSubmit(value: any) {
    this.submit(value).subscribe(() => this.router.navigate([""]));
  }

  deleteUser(userId: number) {
    this.userService
      .deleteUser(userId)
      .subscribe(() => this.router.navigate([""]));
  }

  private submit(value: any) {
    const { navigationId, ...user } = value;
    if (!user.id) {
      return this.userService.createUser(user);
    } else {
      return this.userService.updateUser(user);
    }
  }
}
